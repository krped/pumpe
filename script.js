const h1 = document.querySelector("h1");

const images = document.querySelectorAll(".quiz img");
images.forEach(image => image.addEventListener("click", toggleImage));

const btnCheckResponses = document.querySelectorAll("button.sjekk-svar");
btnCheckResponses.forEach(btn => btn.addEventListener("click", checkResponses));

const btnReadMore = document.querySelectorAll("button.les-mer");
btnReadMore.forEach(btn => btn.addEventListener("click", toggleReadMore));

const infoField = document.querySelectorAll(".info");

let readMore = false;
let lesMerOmTekst = "";
let activeSection = "";

document.addEventListener("keypress", event => {
	activeSection = document.querySelectorAll("section")[event.key].id;
	switch (event.key) {
		case ("1"):
			lesMerOmTekst = "elektrisitet";
			selectSection("Elektrisitet");
			break;
		case ("2"):
			lesMerOmTekst = "hydrogen";
			selectSection("Hydrogen");
			break;
		case ("3"):
			lesMerOmTekst = "biodrivstoff";
			selectSection("Biodrivstoff");
			break;
	}
});

const options = {
	Elektrisitet: {
		color: "hsl(60, 100%, 50%)",
		emoji: `<img src="img/emoji-bolt.png" />`,
		quiz: `<h2>Hvor kan vi få strøm fra?</h2>`
	},
	Hydrogen: {
		color: "hsl(200, 100%, 50%)",
		emoji: `<img src="img/emoji-atom.png" />`,
		quiz: `<h2>Hvilke av disse går på hydrogen?</h2>`
	},
	Biodrivstoff: {
		color: "hsl(110, 50%, 50%)",
		emoji: `<img src="img/emoji-mais.png" />`,
		quiz: `<h2>Hva kan vi lage biodrivstoff av?</h2>`
	}
};

document.querySelectorAll("#intro #drivstoff h2").forEach(h2 => {
	if (h2.innerHTML === "Elektrisitet") {
		h2.style.color = "#000";
	}

	h2.style.backgroundColor = options[h2.innerHTML].color;
	h2.parentElement.style.backgroundColor = options[h2.innerHTML].color;
});

function selectSection(section) {
	readMore = false;
	reset();
	goBackToQuiz();
	
	infoField.forEach(field => {
		field.innerHTML = options[section].quiz;
		field.innerHTML += `<ol><li>Trykk på bildene du tror er riktige.</li>
		<li>Trykk «Sjekk svar <img src="img/emoji-checkbox.png">»-knappen.</li>`;
	});

	document.querySelectorAll("section").forEach(section => section.hidden = true);
	document.querySelector("#" + section.toLowerCase()).removeAttribute("hidden");
	btnReadMore.forEach(btn => btn.innerHTML = `Les mer om ${lesMerOmTekst}`);

	const emoji = options[section].emoji;
	h1.innerHTML = `${emoji} ${section} ${emoji}`;
	h1.style.backgroundColor = options[section].color;

	if (section === "Elektrisitet") {
		h1.style.color = "#000";
	} else {
		h1.style.color = "#fff";
	}
}

function toggleImage() {
	this.selected = !this.selected;

	if (this.selected) {
		this.setAttribute("selected", "");
	} else {
		this.removeAttribute("selected");
		this.removeAttribute("feil-svar");
		this.removeAttribute("riktig-svar");
	}

	const selectedImages = document.querySelectorAll(`section#${activeSection} .quiz img[selected]`);

	let output = "";
	infoField.forEach(field => {
		let id = h1.innerHTML.split(">")[1].split("<")[0].trim();
		let q = options[id].quiz;
		output = q;

		if (selectedImages.length > 0) {
			output += "<ul>";
			selectedImages.forEach(img => {
				output += `<li>${img.name}</li>`;
			});
			output += "</ul>";
		} else {
			output +=
				`<ol>
					<li>Trykk på bildene du tror er riktige.</li>
					<li>Trykk «Sjekk svar <img src="img/emoji-checkbox.png">»-knappen.</li>
				</ol>`;
		}
		field.innerHTML = output;
	});
}

function toggleReadMore() {
	readMore = !readMore;
	const readMoreButtons = document.querySelectorAll("button.les-mer");

	readMoreButtons.forEach(btn => {
		if (readMore) {
			btn.innerHTML = "Gå tilbake til quiz";
		} else {
			btn.innerHTML = `Les mer om ${lesMerOmTekst}`;
		}
	});

	if (readMore) {
		// Skjul .info og .quizbilder
		// Vis .les-mer-tekst
		document.querySelectorAll(".info").forEach(info => {
			info.setAttribute("hidden", "");
		});
		document.querySelectorAll(".quizbilder").forEach(quizbilde => {
			quizbilde.setAttribute("hidden", "");
		});
		document.querySelectorAll(".les-mer-tekst").forEach(lesMerTekst => {
			lesMerTekst.removeAttribute("hidden");
		});
		document.querySelectorAll(".sjekk-svar").forEach(btn => {
			btn.style.visibility = "hidden";
	});

	} else {
		goBackToQuiz();
	}
}

function goBackToQuiz() {
	// Vis .info og .quizbilder
	// Skjul .les-mer-tekst
	document.querySelectorAll(".info").forEach(info => {
		info.removeAttribute("hidden");
	});
	document.querySelectorAll(".quizbilder").forEach(quizbilde => {
		quizbilde.removeAttribute("hidden");
	});
	document.querySelectorAll(".les-mer-tekst").forEach(lesMerTekst => {
		lesMerTekst.setAttribute("hidden", "");
	});
	document.querySelectorAll(".sjekk-svar").forEach(btn => {
		btn.style.visibility = "visible";
	});
}

function checkResponses() {
	document.querySelectorAll("section").forEach(section => {
		if (!section.hidden) {
			const info = document.querySelector(`#${section.id} .info`);
			const selectedImages = section.querySelectorAll(".quiz img[selected]");
			const numberOfCorrectAnswers = section.querySelectorAll(".quiz img[riktig]").length;

			const allSelectedAreCorrect = [...selectedImages].every(img => img.hasAttribute("riktig"));

			const closeButNoCigar =
				allSelectedAreCorrect && (selectedImages.length < numberOfCorrectAnswers);

			const allCorrect =
				allSelectedAreCorrect && (selectedImages.length === numberOfCorrectAnswers);

			if (selectedImages.length > 0) {
				info.innerHTML = "";
				// selectedImages.forEach(image => info.innerHTML = image)

				if (closeButNoCigar) {
					info.innerHTML += "<strong>Fortsett videre! Det finnes flere riktige svar.</strong>";
					selectedImages.forEach(img => {
						info.innerHTML += `<p>${img.getAttribute("svar")}</p>`;
						img.setAttribute("riktig-svar", "");
					});

				} else if (allCorrect) {
					info.innerHTML += "<strong>Alt riktig! Bra jobba.</strong>";
					selectedImages.forEach(img => {
						info.innerHTML += `<p>${img.getAttribute("svar")}</p>`;
						img.setAttribute("riktig-svar", "");
					});

				} else if (!allSelectedAreCorrect) {
					info.innerHTML = `<strong>Ikke riktig.</strong> <button class="restart" onclick="reset()">Prøv på nytt</button>`;
					selectedImages.forEach(img => {
						if (img.hasAttribute("riktig")) {
							img.setAttribute("riktig-svar", "");
						} else {
							img.setAttribute("feil-svar", "");
							info.innerHTML += `<p>${img.getAttribute("svar")}</p>`;
						}
					});
				}
			} else {
				let id = h1.innerHTML.split(">")[1].split("<")[0].trim();
				let q = options[id].quiz;
				info.innerHTML = q;
				info.innerHTML += `<ol><li>Trykk på bildene du tror er riktige.</li>
				<li>Trykk «Sjekk svar <img src="img/emoji-checkbox.png">»-knappen.</li>`;
			}
		}
	});
}

function reset() {
	document.querySelectorAll("img").forEach(img => {
		img.removeAttribute("selected");
		img.removeAttribute("feil-svar");
		img.removeAttribute("riktig-svar");
		img.selected = false;
	});
	infoField.forEach(field => field.innerHTML = "");
	// selectSection(activeSection);
}